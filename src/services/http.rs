use actix_web::{middleware, web, App, HttpResponse, HttpServer};
use log::info;

use crate::produce::produce;
use crate::services::NUMBERS;
use crate::structs::http::HttpPagerMessage;
use crate::structs::kafka::MessageConstructor;

async fn index(item: web::Json<HttpPagerMessage>) -> HttpResponse {
    info!("model: {:?}", &item);
    let (cap_code, frequency) = match NUMBERS.read().unwrap().get_info_pair(item.number) {
        Ok(v) => v,
        Err(_e) => {
            return HttpResponse::BadRequest().json("Number not Found");
        }
    };
    let m = MessageConstructor::default()
        .from("operator".to_string())
        .to_cap(cap_code)
        .text(item.text.clone())
        .freq(frequency)
        .build()
        .unwrap();
    produce(m).await;
    HttpResponse::Ok().json(item.0) // <- send response
}

pub(crate) async fn make_http_server() {
    HttpServer::new(|| {
        App::new()
            // enable logger
            .wrap(middleware::Logger::default())
            .app_data(web::JsonConfig::default().limit(4096)) // <- limit size of the payload (global configuration)
            .service(web::resource("/v1/send").route(web::post().to(index)))
            .service(web::resource("/").route(web::post().to(index)))
    })
    .bind(("0.0.0.0", 8976))
    .unwrap()
    .run()
    .await
    .expect("Fuck");
}
