use std::sync::{Arc, RwLock};

use clap::Parser;
use lazy_static::lazy_static;

lazy_static! {
    pub static ref APP_CONFIG: Arc<RwLock<Cli>> = {
        let n = Cli {
            psb_host: "localhost".to_string(),
            psb_username: None,
            psb_password: "pwd".to_string(),
            topic: "topic".to_string(),
        };
        Arc::new(RwLock::new(n))
    };
}

#[derive(Parser)]
#[command(name = "PagerBot")]
#[command(author = "PushAMP LLC. <ceo@pushamp.com>")]
#[command(version = "0.1.0")]
#[command(about = "Does messages on your pager", long_about = None)]
pub struct Cli {
    #[arg(long, env)]
    psb_host: String,
    #[arg(long, env)]
    psb_username: Option<String>,
    #[arg(long, env)]
    psb_password: String,
    #[arg(long, env, default_value_t = String::from("pager_messages_v1"))]
    topic: String,
}

impl Cli {
    pub(crate) fn get_psb_host(&self) -> &str {
        &self.psb_host
    }

    pub(crate) fn get_psb_topic(&self) -> &str {
        &self.topic
    }

    pub(crate) fn get_psb_username(&self) -> Option<String> {
        self.psb_username.clone()
    }

    pub(crate) fn get_psb_password(&self) -> &str {
        &self.psb_password
    }
}
