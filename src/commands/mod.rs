use teloxide::utils::command::ParseError;
use teloxide::{prelude::*, utils::command::BotCommands};

use send_message::handle_send_message;
use subscribers::handle_subscribers;

pub mod send_message;
mod subscribers;

#[derive(BotCommands, Clone)]
#[command(
    rename_rule = "lowercase",
    description = "These commands are supported:"
)]
pub enum Command {
    #[command(description = "Display text")]
    Help,
    #[command(
    description = "Send message to pager.",
    parse_with = accept_num_and_free_text
    )]
    SendMessage { pager_number: u16, text: String },
    #[command(description = "Show subscribers")]
    Subscribers,
}

fn accept_num_and_free_text(input: String) -> Result<(u16, String), ParseError> {
    let words: Vec<&str> = input.split_whitespace().collect();
    if words.len() < 2 {
        return Err(ParseError::Custom(
            "Type text after number please".to_string().into(),
        ));
    };
    let num = words[0]
        .parse::<u16>()
        .map_err(|e| ParseError::IncorrectFormat(e.into()))?;
    let text = words[1..].join(" ");
    Ok((num, text))
}

pub async fn answer(bot: Bot, msg: Message, cmd: Command) -> ResponseResult<()> {
    match cmd {
        Command::Help => {
            bot.send_message(msg.chat.id, Command::descriptions().to_string())
                .await?;
        }
        Command::SendMessage { pager_number, text } => {
            handle_send_message(bot, msg, pager_number, text).await?
        }
        Command::Subscribers => handle_subscribers(bot, msg).await?,
    };
    Ok(())
}
