use teloxide::prelude::Requester;
use teloxide::prelude::*;

use crate::NUMBERS;

pub async fn handle_subscribers(bot: Bot, message: Message) -> ResponseResult<()> {
    let t = NUMBERS.read().unwrap().subscribers();
    bot.send_message(message.chat.id, t).await?;
    Ok(())
}
