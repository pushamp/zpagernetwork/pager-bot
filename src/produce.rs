use std::time::Duration;

use log::info;
use rdkafka::config::ClientConfig;
use rdkafka::message::{Header, OwnedHeaders};
use rdkafka::producer::{FutureProducer, FutureRecord};
use serde::Serialize;

use crate::app::APP_CONFIG;

pub async fn produce<T>(message: T)
where
    T: PartialEq<T> + Serialize,
{
    // r.get_psb_host();
    let producer: &FutureProducer = &ClientConfig::new()
        .set(
            "bootstrap.servers",
            APP_CONFIG.read().unwrap().get_psb_host(),
        )
        .set("security.protocol", "SASL_SSL")
        .set("sasl.mechanism", "SCRAM-SHA-512")
        .set(
            "sasl.username",
            APP_CONFIG
                .read()
                .unwrap()
                .get_psb_username()
                .unwrap_or("pager_bot".to_string()),
        )
        .set(
            "ssl.ca.location",
            "/usr/local/share/ca-certificates/Yandex/YandexInternalRootCA.crt",
        )
        .set(
            "sasl.password",
            APP_CONFIG.read().unwrap().get_psb_password(),
        )
        .set("message.timeout.ms", "5000")
        .create()
        .expect("Producer creation error");
    let s = match serde_json::to_string(&message) {
        Ok(v) => v,
        Err(_) => return,
    };
    let h = Header {
        key: "header_key",
        value: Some("header_value"),
    };
    let topic_name = APP_CONFIG.read().unwrap().get_psb_topic().to_string();
    let delivery_status = producer
        .send(
            FutureRecord::to(&topic_name)
                .payload(&s)
                .key(&"".to_string())
                .headers(OwnedHeaders::new().insert(h)),
            Duration::from_secs(0),
        )
        .await;

    // This will be executed when the result is received.
    info!("Delivery status for message {:?} received", delivery_status);
}
