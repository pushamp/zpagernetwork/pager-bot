use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct HttpPagerMessage {
    pub text: String,
    pub number: u16,
}
