use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Builder)]
#[builder(name = "MessageConstructor")]
pub struct Message {
    pub from: String,
    pub to_cap: String,
    pub text: String,
    pub freq: u32,
}
