#[macro_use]
extern crate derive_builder;

use clap::Parser;
use log::info;
use rdkafka::util::get_rdkafka_version;

use crate::app::{Cli, APP_CONFIG};
use crate::services::{start_http, start_numbers_loop, start_tgbot, NUMBERS};

mod app;
mod commands;
mod produce;
mod services;
mod structs;

#[tokio::main]
async fn main() {
    pretty_env_logger::init();
    let cli = Cli::parse();
    let (version_n, version_s) = get_rdkafka_version();
    info!("rd_kafka_version: 0x{:08x}, {}", version_n, version_s);
    {
        let mut d = APP_CONFIG.write().unwrap();
        *d = cli;
    }
    start_numbers_loop();
    start_tgbot();
    start_http().await
}
